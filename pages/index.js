import React from "react";
import { Layout } from "../components/Layout.js";
import {AcmeLogo} from "../components/AcmeLogo";
import {Button, Text, Navbar} from "@nextui-org/react";
import Link from "next/link";
import { Dropdown } from "@nextui-org/react";

function HomePage(){
    const [variant, setVariant] = React.useState("static");
    const variants = ["static", "floating", "sticky"];

    return (
        <Layout>

        <Navbar isBordered varifant={variant}>
            <Navbar.Brand>
                <AcmeLogo/>
                <Text size={30} css={{
                    textGradient: "45deg, $blue600 -20%, $red600 50%",
                }} b color="inherit" hideIn="xs">
                    Tilesko
                </Text>
            </Navbar.Brand>
            <Dropdown>
                <Dropdown.Button flat>Namespaces</Dropdown.Button>
                <Dropdown.Menu aria-label="Static Actions">
                    <Dropdown.Item key="new"></Dropdown.Item>
                    <Dropdown.Item key="copy"></Dropdown.Item>
                    <Dropdown.Item key="edit"></Dropdown.Item>
                    <Dropdown.Item key="delete" color="error"> </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
            <Navbar.Content hideIn="xs">
                <Link href='/' color="secondary" block="true">Home</Link>
                <Link href='/table' color="secondary" block="true">Table</Link>
            </Navbar.Content>
            <Navbar.Content>
                <Navbar.Link color="inherit" href="#">
                    Login
                </Navbar.Link>
                <Navbar.Item>
                    <Button auto flat as={Link} href="#">
                        Sign Up
                    </Button>
                </Navbar.Item>
            </Navbar.Content>
        </Navbar>
        </Layout>
    );
}
export default HomePage;
