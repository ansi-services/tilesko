## Getting Started

First, run the development server:

```bash
skaffold dev
```

To open app un browser, you have to port-forward the apps port expossed by Kubernetes:

```bash
kubectl port-forward service/tilesko 3000:3000
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

Skaffold automatically deploys the app to Kubernetes and watches for changes to the source code. When you make changes to the source code, Skaffold automatically rebuilds and deploys the app to Kubernetes.
