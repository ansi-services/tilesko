export const environment = {
    token: '',
    namespacesUrl: '/api/v1/namespaces',
    dashboardUrl: '/apis/tilesko.ansi.services/v1/namespaces/default/dashboards',
    resourcesUrl: '/apis/mast.ansi.services/v1/namespaces/default/mastjobs',
    production: false,
};
