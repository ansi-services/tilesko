
export class KubernetesResourceList {
    kind: string;
    apiVersion: string;
    metadata: Metadata;
    items: Item[];
}

export class Metadata {
    resourceVersion: string;
}

export class Item {
    metadata: Metadata2;
    spec: {};
    status: Status;
}

export class Metadata2 {
    name: string;
    uid: string;
    resourceVersion: string;
    creationTimestamp: string;
    labels: Labels;
    managedFields: ManagedField[];
}

export class Labels {
    'kubernetes.io/metadata.name': string;
}

export class ManagedField {
    manager: string;
    operation: string;
    apiVersion: string;
    time: string;
    fieldsType: string;
}



export class Status {
    phase: string;
}
